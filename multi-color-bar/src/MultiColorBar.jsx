import { Component, createElement } from "react";
import { hot } from "react-hot-loader/root";

import Bar from "./components/Bar";
import "./ui/MultiColorBar.css";

class MultiColorBar extends Component {
    constructor(props) {
        super(props);

        this.state = {
            visualParts: []
        }

    }

    componentDidMount() {

        
        
        const loader = setInterval(() => {
            let { valueAttribute1, valueAttribute2, valueAttribute3 } = this.props;
            const visualParts = [];
            
            if (
                valueAttribute1.status === "available" &&
                valueAttribute2.status === "available" &&
                valueAttribute3.status === "available"
            ) {

              
                    visualParts.push(
                        {
                            percentage: `${valueAttribute1.displayValue}%`,
                            color: "#45a400"
                        }
                    )
                
                    visualParts.push(
                        {
                            percentage: `${valueAttribute2.displayValue}%`,
                            color: "#fa363d"
                        }
                    )
                

                    visualParts.push(
                        {
                            percentage: `${valueAttribute3.displayValue}%`,
                            color: "#d9d9d9"
                        }
                    )
                

                clearInterval(loader);
                this.setState({ visualParts })
            }

        }, 300)

    }

    renderHeader(type = "normal") {

        const { optionsCount, countGray, countGreen, countRed } = this.props;

        if (type === "sideToside") {

            return (
                <div style={{ display: "flex" }}>
                    <div style={{ flex: 1, textAlign: "left" }}>
                        <p className={"style-card-title"} style={{ fontSize: "1rem" }}>OPTIONS</p>
                        <p style={{ color: "#333", fontWeight: "bold", fontSize: "1.5rem" }}>{optionsCount.displayValue}</p>
                    </div>
                    <div style={{ flex: 1, textAlign: "right" }}>
                        <p className={"style-card-title"} style={{ fontSize: "1rem" }}>STATUS</p>
                        <p style={{ color: "#ccc" }}>
                            <p>
                                <b style={{ color: "#45a400", fontWeight: "bold", fontSize: "1.5rem" }}>{countGreen.displayValue}</b>/
                                <b style={{ color: "#fa363d", fontWeight: "bold", fontSize: "1.5rem" }}>{countRed.displayValue}</b>/
                                <b style={{ color: "#333", fontWeight: "bold", fontSize: "1.5rem" }}>{countGray.displayValue}</b>
                            </p>
                        </p>
                    </div>
                </div>
            )

        } else {
            return (
                <p style={{ color: "#333" }}>
                    {optionsCount && <b style={{
                        color: "#333",
                        fontSize: "1.6rem"
                    }}>{optionsCount.displayValue}</b>} (<b style={{ color: "#45a400" }}>{countGreen.displayValue}</b>/<b style={{ color: "#fa363d" }}>{countRed.displayValue}</b>/
                    <b style={{ color: "#333" }}>{countGray.displayValue}</b>)
                </p>
            )
        }
    }

    render() {

        if (this.state.visualParts.length === 0) {
            return "";
        }


        return (
            <div style={{ marginTop: 5, marginBottom: 5 }}>
                {this.renderHeader(this.props.layout)}
                <Bar
                    visualParts={this.state.visualParts}
                />
            </div>
        )
    }
}


export default hot(MultiColorBar);
