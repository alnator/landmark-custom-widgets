import { Component, createElement } from "react";
import { hot } from "react-hot-loader/root";

import { BadgeSample } from "./components/BadgeSample";
import "./ui/ImageUrlViewer.css";

class ImageUrlViewer extends Component {
    constructor(props) {
        super(props);
      
    }

    render() {
        return (
            <div>
                <img src={`/milestone_icons/${this.props.image.displayValue}.svg`} className="pull-right" style={{
                    marginRight: "1rem",
                    marginTop:"1rem"
                }} />
           </div>
        );
    }

  
}

export default hot(ImageUrlViewer);
