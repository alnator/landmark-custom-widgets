import { Component, createElement } from "react";
import { View, WebView } from "react-native";

import { styles } from "../ui/styles";
import { flattenStyles } from "../utils/common";

const defaultBadgeStyle = {
    container: styles.flex,
    badge: styles.badge,
    label: styles.label
};

export class MxWebView extends Component {
    styles = flattenStyles(defaultBadgeStyle, this.props.style);

    render() {
        return (
            <View style={this.styles.container}>
                <WebView
                    source={{
                        uri: this.props.uri
                    }}
                    style={{ margin: 5 }}
                />
            </View>
        );
    }
}
