import { Component, createElement } from "react";
import WebView from "./components/WebView";

export class WebViewWidget extends Component {
    constructor(props) {
        super(props);

        this.onClickHandler = this.onClick.bind(this);
    }

    render() {
        return <WebView uri={this.props.uri.displayValue} />;
    }

    onClick() {
        if (this.props.onClickAction) {
            this.props.onClickAction.execute();
        }
    }
}
