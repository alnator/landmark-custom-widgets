const mapping = {
    "po_supplierasn": {
        "shipped_to_cfs": {
            "fields": {
                "current_status": true,
                // "asn_no:": true,
                // "asn_date:": true,
                "next_action": true
            }
        },
        "po-initiated": {
            "fields": {
                "current_status": true,
                // "upload_ref:": true,
                // "upload_date:": true,
                "next_action": true
            }
        },
        "pricing_approved": {
            "fields": {
                "current_status": true,
                // "upload_ref:": true,
                // "pricing_date:": true,
                "next_action": true
            }
        },
        "po_approved": {
            "fields": {
                "current_status": true,
                // "po_no:": true,
                // "po_approval_date:": true,
                "next_action": true
            }
        },
        "packing_list_created": {
            "fields": {
                "current_status": true,
                // "packing_list_no:": true,
                // "packing_list_date:": true,
                "next_action": true
            }
        },
        "asn_created": {
            "fields": {
                "current_status": true,
                // "asn_no:": true,
                // "asn_date:": true,
                "next_action": true
            }
        },
        "supplier_shipped_out": {
            "fields": {
                "current_status": true,
                // "asn_no:": true,
                // "asn_date:": true,
                "next_action": true
            }
        },
        "shipped_out": {
            "fields": {
                "current_status": true,
                // "asn_no:": true,
                // "asn_date:": true,
                "next_action": true
            }
        },
        "pending_receipt": {
            "fields": {
                "current_status": true,
                // "asn_no:": true,
                // "expected_date:": true,
                "next_action": true
            }
        },
        "received": {
            "fields": {
                "current_status": true,
                // "asn_no:": true,
                // "receipt_date:": true,
                "next_action": true
            }
        },
        "cfs_asn_created": {
            "fields": {
                "current_status": true,
                // "cfs_asn_no:": true,
                "next_action": true
            }
        },
        "cfs_shipped_out": {
            "fields": {
                "current_status": true,
                // "cfs_asn_no:": true,
                // "ship_date:": true,
                "next_action": true
            }
        },
        "cdc-in_clearance_initiated": {
            "fields": {
                "current_status": true,
                // "cdc_job_no:": true,
                "next_action": true
            }
        },
        "cdc-_in_clearance_started": {
            "fields": {
                "current_status": true,
                // "cdc_job_no:": true,
                // "do_date:": true,
                "next_action": true
            }
        },
        "cdc-in_clearance_completed": {
            "fields": {
                "current_status": true,
                // "cdc_job_no:": true,
                // "cleared_date:": true,
                "next_action": true
            }
        },
        "cdc-in_-deliver_plan_initiated": {
            "fields": {
                "current_status": true,
                // "delivery_id:": true,
                "next_action": true
            }
        },
        "cdc-_in-delivery_planned": {
            "fields": {
                "current_status": true,
                // "delivery_id:": true,
                // "expect_date:": true,
                "next_action": true
            }
        },
        "cdc-in_shipment_received": {
            "fields": {
                "current_status": true,
                // "asn_no:": true,
                // "cdc_receipt_date:": true,
                "next_action": true
            }
        },
        "cdc-out_delivery_planned": {
            "fields": {
                "current_status": true,
                // "cust_invoice_no:": true,
                "next_action": true
            }
        },
        "shipped_to_rdc": {
            "fields": {
                "current_status": true,
                // "cust_invoice_no:": true,
                // "rdc_ship_date:": true,
                "next_action": true
            }
        },
        "rdc-in_clearance_initiated": {
            "fields": {
                "current_status": true,
                // "rdc_job_no:": true,
                "next_action": true
            }
        },
        "rdc-_in_clearance_started": {
            "fields": {
                "current_status": true,
                // "rdc_job_no:": true,
                // "do_date:": true,
                "next_action": true
            }
        },
        "rdc-in_clearance_completed": {
            "fields": {
                "current_status": true,
                // "rdc_job_no:": true,
                // "cleared_date:": true,
                "next_action": true
            }
        },
        "rdc-in_-deliver_plan_initiated": {
            "fields": {
                "current_status": true,
                // "delivery_id:": true,
                "next_action": true
            }
        },
        "rdc-_in-delivery_planned": {
            "fields": {
                "current_status": true,
                // "delivery_id:": true,
                // "expect_date:": true,
                "next_action": true
            }
        },
        "rdc-in_shipment_received": {
            "fields": {
                "current_status": true,
                // "cust_invoice_no:": true,
                // "rdc_receipt_date:": true,
                "next_action": true
            }
        },

        "pending_receipt_in_cfs": {
            "fields": {
                "current_status": true,
                // "asn_no:": true,
                "next_action": true
            }
        },
        "receive_in_cfs": {
            "fields": {
                "current_status": true,
                // "asn_no:": true,
                // "expected_date:": true,
                "next_action": true
            }
        },
        "cfs_asn_created": {
            "fields": {
                "current_status": true,
                // "receipt_date:": true,
                "next_action": true
            }
        },
        "cfs_asn_shipped_out": {
            "fields": {
                "current_status": true,
                // "cfs_asn_no:": true,
                // "ship_date:": true,
                "next_action": true
            }
        },
        "cdc_invoice_created": {
            "fields": {
                "current_status": true,
                // "cust_invoice_no:": true,
                // "created_date:": true,
                "next_action": true
            }
        },
        "rdc-in_clearance_initiated": {
            "fields": {
                "current_status": true,
                // "rdc_job_no:": true,
                "next_action": true
            }
        },
        "rdc-in_clearance_started": {
            "fields": {
                "current_status": true,
                // "rdc_job_no:": true,
                // "do_date:": true,
                "next_action": true
            }
        },
        "rdc-in_clearance_completed": {
            "fields": {
                "current_status": true,
                // "rdc_job_no:": true,
                // "created_date:": true,
                "next_action": true
            }
        },
        "rdc-in-deliver_plan_initiated": {
            "fields": {
                "current_status": true,
                // "delivery_id:": true,
                "next_action": true
            }
        },
        "rdc-in-delivery_planned": {
            "fields": {
                "current_status": true,
                // "delivery_id:": true,
                // "expect_date:": true,
                "next_action": true
            }
        },
        "rdc-in_shipment_received": {
            "fields": {
                "current_status": true,
                // "cust_invoice_no:": true,
                // "rdc_receipt_date:": true,
                "next_action": true
            }
        }
    }
}




export default class AttributesMapper {
    constructor(attr, milestoneId, onlyClean = false) {
        this.attr = attr;
        this.milestoneId = milestoneId;
        this.cleanedAttr = null;
        this.attrToHighlight = [];
        this.restAttrs = [];
        this.onlyClean = onlyClean;
    }

    init() {
        this.cleanedAttr = this.reduceAttributes(this.attr);

        for (const attr in this.cleanedAttr) {
            if (this.onlyClean) {
                this.restAttrs.push({ [attr]: this.cleanedAttr[attr] });
            } else {
                if (this.isMappingField(this.milestoneId, this.cleanedAttr["current_status"], attr)) {
                    this.attrToHighlight.push({ [attr]: this.cleanedAttr[attr] });
                } else {
                    this.restAttrs.push({ [attr]: this.cleanedAttr[attr] });
                }
            }
        }

        return {
            hightlight: this.attrToHighlight,
            rest: this.restAttrs
        }
    }

    reduceAttributes(attr) {
        return attr.reduce((acc, item) => {

            acc[item.Name.value.toLowerCase().replace(/ /g, '_')] = {
                value: item.Value.value.toLowerCase().replace(/ /g, '_'),
                displayValue: item.Value.value
            }
            return acc;
        }, {})
    }

    isMappingField(milestoneId, status, field) {

        try {
            if (mapping.hasOwnProperty(milestoneId)) {
                //if (typeof mapping[milestoneId][status.value] !== undefined || typeof mapping[milestoneId][status.value] !== "undefined") {
                    const fields =  [
                       
                            "current_status",
                            "next_action"                      
                    ]

                    return fields.includes(field) || fields.includes(`${field}:`);
                // } else {
                //     console.log("no mapping for status")
                //     return false;
                // }
            } else {
                console.log("no mapping for milestone");
                return false;
            }
        } catch (e) {
            console.warn("Error in isMappingField Function : " + e);
        }
    }
}
