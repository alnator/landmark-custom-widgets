import React, { createElement } from "react";
import classNames from "classnames";
import { FaChevronDown, FaChevronUp, FaChevronRight, FaRegStar } from 'react-icons/fa';

import MileStoneNotification from './MileStoneNotification';
import AttributesMapper from './mapping';

const capitalize = (s) => {
    if (typeof s !== 'string') return ''
    return s.charAt(0).toUpperCase() + s.slice(1)
}

export default function MileStoneAttributes(props) {
    const [mappingField, setMappingField] = React.useState([]);
    const [isOpen, setIsOpen] = React.useState(false);
    const list = React.useRef();


    function renderAttr(attrs, useHeader = false) {
        return (
            <React.Fragment>
                <span onClick={() => setIsOpen(!isOpen)} style={{ position: "absolute", top: "2rem", right: "1rem" }}>
                    {isOpen === false ? <FaChevronRight style={{ color: "#ccc" }} /> : <FaChevronDown style={{ color: "#ccc" }} />}
                </span>
                <div style={{ display: isOpen === true ? 'block' : 'none' }}>
                    <ul ref={list} style={{ display: isOpen === true ? 'block' : 'none', marginTop: "1rem" }}>
                        { useHeader && <React.Fragment>
                            <li>
                                <span style={{ fontSize: "1rem",color:"#007aff" }}>Current Status</span>:{'    '}{props.axtraAttrs.current_status}
                            </li>
                            <li>
                                <span style={{ fontSize: "1rem",color:"#007aff" }}>Next Action</span>:{'    '}{props.axtraAttrs.next_action}
                            </li>
                        </React.Fragment>

                        }
                        {
                            attrs.map(
                                attr => {
                                    //console.log("name", attr.Name.value, "value", attr.Value.value, attr.Value.value !== null, allowedAtrributes.includes(attr.Name.value))
                                    //if (attr.Value.value !== null && allowedAtrributes.includes(attr.Name.value.toUpperCase().replace(/ /g, "_"))) {  
                                    if (attr.Value.value.indexOf("http") >= 0) {
                                        return <li style={{
                                            listStyle: "none",
                                            fontSize: "0.8em",

                                            marginTop: "0.5em",
                                            marginLeft: "-1.5rem"
                                        }}><button style={{
                                            color: " #007aff",

                                            border: "1px solid #007aff",
                                            borderRadius: "5px",
                                            padding: "0.4rem 1.9rem 0.4rem 1.9rem"
                                        }} onClick={() => fetchCertificateURL(attr.Value.value, token.displayValue, conceptId.displayValue)}><FaRegStar style={{ marginRight: "0.5rem" }} />{attr.Name.value.replace(/_/g, ' ')}</button></li>
                                    }

                                    if (attr.Value.value !== null) {
                                        return <li><b>{attr.Name.value.replace(/_/g, ' ')}</b>:{' '}{attr.Value.value}</li>
                                    }


                                }
                            ) || "no attributes"
                        }
                    </ul>

                    {canNotify.displayValue === "Yes" && <MileStoneNotification {...props} />}
                </div>

            </React.Fragment>
        )
    }


    function renderCleanedAttr(attrs, Hightlight) {
        console.log(Hightlight)
        return (
            <React.Fragment>
                <span onClick={() => setIsOpen(!isOpen)} style={{ position: "absolute", top: "2rem", right: "1rem" }}>
                    {isOpen === false ? <FaChevronRight style={{ color: "#ccc" }} /> : <FaChevronDown style={{ color: "#ccc" }} />}
                </span>
                <div style={{ display: isOpen === true ? 'block' : 'none' }}>
                    {Hightlight}
                    <ul ref={list} style={{ display: isOpen === true ? 'block' : 'none' }}>
                        {
                            attrs.map(
                                attr => {
                                    //console.log("name", attr.Name.value, "value", attr.Value.value, attr.Value.value !== null, allowedAtrributes.includes(attr.Name.value))
                                    //if (attr.Value.value !== null && allowedAtrributes.includes(attr.Name.value.toUpperCase().replace(/ /g, "_"))) {  
                                    if (attr[Object.keys(attr)[0]].displayValue.indexOf("http") >= 0) {
                                        return <li style={{
                                            listStyle: "none",
                                            fontSize: "0.8em",

                                            marginTop: "0.5em",
                                            marginLeft: "-1.5rem"
                                        }}><button style={{
                                            color: " #007aff",

                                            border: "1px solid #007aff",
                                            borderRadius: "5px",
                                            padding: "0.4rem 1.9rem 0.4rem 1.9rem"
                                        }} onClick={() => fetchCertificateURL(attr[Object.keys(attr)[0]].displayValue, token.displayValue, conceptId.displayValue)}><FaRegStar style={{ marginRight: "0.5rem" }} />{Object.keys(attr)[0].replace(/_/g, ' ')}</button></li>
                                    }

                                    if (attr[Object.keys(attr)[0]].displayValue !== null) {
                                        return <li><b>{capitalize(Object.keys(attr)[0].replace(/_/g, ' '))}</b>:{' '}{checkToSplit(attr[Object.keys(attr)[0]].displayValue)}</li>
                                    }


                                }
                            ) || "no attributes"
                        }
                    </ul>

                    {canNotify.displayValue === "Yes" && <MileStoneNotification {...props} />}
                </div>

            </React.Fragment>
        )
    }

    function checkToSplit(value) {

        if (value.indexOf(',') >= 0) {
            const values = value.split(',');
            return (
                <span>
                    <ul>
                        {
                            values.map(val => <li><i>{val}</i></li>)
                        }
                    </ul>
                </span>
            )
        }

        return <span><i>{value}</i></span>

    }

    function renderHighLightedAttr(attrs) {
        let next_action = false;
        return (
            <React.Fragment>

                {attrs.length > 0 && <div >
                    <ul ref={list} style={{ marginTop: "1rem" }}>
                        {
                            attrs.map(
                                attr => {
                                    if (Object.keys(attr)[0].replace(/ /g, '_').toLowerCase() !== "next_action") {
                                        if (Object.keys(attr)[0].replace(/ /g, '_').toLowerCase() === "current_status") {

                                            return (
                                                <li>
                                                    <span style={{ fontSize: "1rem",color:"#007aff" }}>{capitalize(Object.keys(attr)[0].replace(/_/g, ' ').toUpperCase())}</span>:{'    '}{props.axtraAttrs.current_status || attr[Object.keys(attr)[0]].displayValue}
                                                </li>
                                            )
                                        }
                                        return (
                                            <li>
                                                <span style={{ fontSize: "1rem" }}>{capitalize(Object.keys(attr)[0].replace(/_/g, ' ').toUpperCase())}</span>:{'    '}{checkToSplit(attr[Object.keys(attr)[0]].displayValue)}
                                            </li>
                                        )
                                    } else {
                                        next_action = attr;
                                        return ""
                                    }
                                }
                            ) || ""
                        }
                        {next_action && <li>
                            <span style={{ fontSize: "1rem", color:"#007aff" }}>{Object.keys(next_action)[0].replace(/_/g, ' ').toUpperCase()}</span>:{'    '}{props.axtraAttrs.next_action || checkToSplit(next_action[Object.keys(next_action)[0]].displayValue)}
                        </li>}
                    </ul>
                </div>}

            </React.Fragment>
        )
    }

    async function fetchCertificateURL(url, token, concept) {
        const certificateWindow = window.open("")
        const Url = await fetch(`https://apidev.landmarkgroup.com/landmarkgroup/retail-dev/v2/sahla-buy/milestones/media/stream/concepts/${concept}?mediaUrl=${url}`, {
            method: "GET",
            mode: 'cors',
            headers: {
                "Authorization": `Bearer ${token}`,
                "x-ibm-client-id": "26adb636-34a4-41b2-80fb-e727af79111a",
                "access-token": token,
                user_id: "STEP",
                pswd: "sahlabuy@2019"
            }
        })
            .then(response => response.blob())
            .then((myBlob) => {
                var file = new Blob([myBlob], { type: 'application/pdf' });
                var objectURL = URL.createObjectURL(file);
                certificateWindow.location = objectURL;

                //window.open(objectURL)
            });
        return Url;
    }


    const { attributes, allowedAtrributes, canNotify, milestone, showLocations, conceptId, token } = props;

    if (milestone.MilestoneId.value === "supplierasn_import") {
        return (
            <React.Fragment>
                <span onClick={() => showLocations()} style={{ position: "absolute", top: "2rem", right: "1rem" }}>
                    {isOpen === false ? <FaChevronRight style={{ color: "#ccc" }} /> : <FaChevronDown style={{ color: "#ccc" }} />}
                </span>
            </React.Fragment>
        )

    } else if (milestone.MilestoneId.value === "po_supplierasn") {

        /* Apply An Atrributes Mapper if needed */
        // console.log(attributes)
        // const Mapper = new AttributesMapper(attributes, milestone.MilestoneId.value);
        // const { hightlight, rest } = Mapper.init();

    
        // return (
        //     <React.Fragment>
        //         {renderCleanedAttr(rest, renderHighLightedAttr(hightlight))}
        //     </React.Fragment>

        // )
        return renderAttr(attributes,true);
    } else {
        return renderAttr(attributes);
    }
}