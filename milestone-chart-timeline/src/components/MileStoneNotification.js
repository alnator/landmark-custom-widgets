import React, { createElement } from "react";
import classNames from "classnames";
import swal from 'sweetalert';
import Swal from 'sweetalert2'
import NotifiyIcon from '../ui/notify_icon.svg'

export default function MileStoneNotification(props) {

    function getNotificationMsg(owner_of_milestone, milestone_name, style_name, schedule_date, buyer_name) {
        return `
            Dear ${owner_of_milestone},
            please note that milestone ${milestone_name}
            on style ${style_name} is due to be completed on ${schedule_date}.
            Request your attention on completing the activity on time. Thanks
        `;
    }

    function buildMsgObject() {
        const { milestone, conceptId, HitID, metadata,StyleName } = props;
        const StyleId = milestone.StyleLMId.displayValue;
        const Concept = conceptId.displayValue;
        


        const userRoles = metadata.UserRole;

        const request = [];
        for (let index = 0; index < userRoles.length; index++) {
            const role = userRoles[index];
            if (role !== "") {

                const msg = getNotificationMsg(
                    role,
                    metadata.Description,
                    //milestone.MilestoneId.value,
                    //milestone.StyleLMId.displayValue,
                    StyleName,
                    milestone.ScheduleCompletion.value,
                    role

                )

                request.push({
                    "Message": msg,
                    "UserRole": role,
                    "SeasonID": milestone.SeasonID.displayValue,
                    StyleId,
                    Concept,
                    HitID: HitID.displayValue
                })
            }

        }

        return request;

    }


    async function submitNotification() {
        const {resizeWindow} = props;
        let body = buildMsgObject();

        Swal.mixin({
            input: 'textarea',
            focusConfirm: false,
            confirmButtonText: 'Submit Notification &rarr;',
            showCancelButton: true,
            progressSteps: ['1', '2'],
            inputValue: body[0].Message
        }).queue([
            {
                title: 'Notification',
                text: 'Notification body, if you like to add anything !',
                inputValue: body[0].Message
            }
        ]).then((result) => {
            if (result.value) {
                const answers = result.value

                body = body.map(item => {
                    return {
                        ...item,
                        Message: answers[0]
                    }
                });



                fetch('/v1/notification/Notification',
                    {
                        method: "POST",
                        headers: {
                            "accept": "application/json",
                            "Content-Type": "application/json"
                        },
                        body: JSON.stringify(body)
                    }
                ).catch(e => console.warn(e))
                    .then(() => { 
                        Swal.fire("Thank You", "Notification Submited", "success");
                        resizeWindow();
                    });
            }
        })


        // console.log(body)

    }

    return (
        <div>
            <button
                onClick={() => submitNotification()}
                style={{
                    background: "#007aff",
                    color: "#FFF",
                    fontSize: "1.1rem",
                    padding: "0.4rem",
                    marginTop: "1rem",
                    marginBottom: "1rem",
                    borderRadius: "5px",
                    outline: "none",
                    border: "none",
                    borderStyle: "none"

                }}>
                <img src={NotifiyIcon} alt="icon" style={{
                    marginRight: "1rem",
                    marginTop: "3px",
                    marginleft: "3px"
                }} />
                SEND NOTIFICATION
            </button>
        </div>
    )
}