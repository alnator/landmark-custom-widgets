// TODO : converting getting object to update state after each promise to look like a lazy load 
// TODO : lazy load attributes promises
// TODO : DATE BATCH TO BE MOVED TO BACKEND AND CALLED AS A MICROFLOW

import React, { Component, createElement } from "react";
import { hot } from "react-hot-loader/root";
import { getObject } from "@jeltemx/mendix-react-widget-utils";
import { VerticalTimeline, VerticalTimelineElement } from "./components/VerticalTimeline";
import MileStoneAttributes from './components/MileStoneAttributes';
import MileStoneNotification from './components/MileStoneNotification';

import { FaCalendarPlus, FaArrowLeft } from 'react-icons/fa';
import Loader from 'react-loader-spinner';

import {
    DatePicker,
    MuiPickersUtilsProvider,
} from "@material-ui/pickers";
import DateFnsUtils from '@date-io/date-fns';

import 'react-calendar/dist/Calendar.css';
import "./ui/MilestoneChartWidget.css";
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css"

//////////////////////////////////////////
// Helpers and Constants
//////////////////////////////////////////

const Months = {
    "01": "January",
    "02": "February",
    "03": "March",
    "04": "April",
    "05": "May",
    "06": "June",
    "07": "July",
    "08": "August",
    "09": "September",
    "10": "October",
    "11": "November",
    "12": "December"
}

Date.prototype.monthNames = [
    "January", "February", "March",
    "April", "May", "June",
    "July", "August", "September",
    "October", "November", "December"
];

Date.prototype.getMonthName = function () {
    return this.monthNames[this.getMonth()];
};
Date.prototype.getShortMonthName = function () {
    return this.getMonthName().substr(0, 3);
};

function hasNumber(myString) {
    return /\d/.test(myString);
}

const COLORS = {
    blue: "#0082ff",
    red: "#fa363d",
    green: "#45a400"
};

const STATUS_COLOR = {
    RISK: COLORS.red,
    ON_TRACK: COLORS.green
};

// constant tracking of the today mark was rendered before
let todayRendered = false;


/**
 * Beautify any date to short format
 *
 * @param {*} date
 * @returns
 */
function beautify(date) {

    if (date) {
        const actualDate = date.split("-");
        const day = actualDate[2];
        const month = Months[actualDate[1]];
        const year = actualDate[0];

        const dateObject = new Date(`${month} ${day}, ${year}`)

        return `${day}${dateObject.getShortMonthName()}\`${year.slice(-2)}`
    }

    return ""
}



/**
 * Today Icon for timeline
 *
 * @param {*} { day, mon }
 * @returns
 */
const TodayIcon = ({ day, mon }) => {
    return (
        <div
            style={{
                lineHeight: 0.1,
                textAlign: "center"
            }}
        >
            <p style={{ fontWeight: "bold", color: COLORS.blue, fontSize: 25 }}>{day}</p>
            <p style={{ fontWeight: "normal", color: COLORS.blue, fontSize: 12 }}>{mon}</p>
        </div>
    );
}

/**
 * Date Icon Component
 *
 * @param {*} { status, date, revisedDate, scheduleCompletion, onClick }
 * @returns
 */
const IconDate = ({ status, date, revisedDate, scheduleCompletion, onClick }) => {

    let actualDate;
    let color;
    let day = 0
    let month;
    let year;
    let dateObject;

    // default 
    color = COLORS.red;


    try {

        if (date !== null) {
            actualDate = date.split("-");
            console.log(actualDate)
            day = actualDate[2];
            month = Months[actualDate[1]];
            year = actualDate[0];

            dateObject = new Date(`${month} ${day}, ${year}`)
            color = STATUS_COLOR[status] || COLORS.blue;
        } else {
            return <FaCalendarPlus style={{ color: "#ccc" }} />
        }

    } catch (e) {
        return <FaCalendarPlus style={{ color: "#ccc" }} />
    }

    if (!day) {
        return <FaCalendarPlus style={{ color: "#ccc" }} />
    }

    return (
        <div
            style={{
                lineHeight: 0.1,
                textAlign: "center"
            }}
            onClick={onClick ? () => onClick() : null}
        >
            <p style={{ fontWeight: "bold", color, fontSize: 25 }}>{day}</p>
            <p style={{ fontWeight: "normal", color, fontSize: 12 }}>{dateObject.getShortMonthName()}</p>
        </div>
    );

}


class MilestoneChartWidget extends Component {
    constructor(props) {
        super(props);

        this.state = {
            loaded: false,
            timeline: [],
            metadata: 0,
            attributes: false,
            attributesLoaded: false,
            todayRendered: false,
            date: new Date(),
            showModal: false,
            activeMilestone: false,
            patchedMilestones: false,
            allowedAtrributes: false,
            showLocations: false,
            selectedDate: new Date()
        };

        this.handleOpenModal = this.handleOpenModal.bind(this);
        this.handleCloseModal = this.handleCloseModal.bind(this);

        this.datePicker = React.createRef();
        this.containerDev = React.createRef();
    }

    /**
     * get a mendix attribute passed to the widget when the status is ready
     *
     * @param {*} propName
     * @param {*} stateKey
     * @param {boolean} [isList=false]
     * @param {boolean} [splitBy=false]
     * @param {boolean} [modifier=false]
     * @returns
     * @memberof MilestoneChartWidget
     */
    async getItemsFromListWhenReady(propName, stateKey, isList = false, splitBy = false, modifier = false) {

        return new Promise((resolve, reject) => {
            try {
                const dataLoaderInterval = setInterval(() => {
                    const { status } = this.props[propName];

                    if (status === "available") {

                        clearInterval(dataLoaderInterval);

                        if (isList) {
                            this.loadItems(this.props[propName].items, stateKey)
                                .then(_ => resolve(_));
                        } else {
                            let data = this.props[propName].displayValue;

                            if (splitBy) {
                                data = data.split(splitBy);
                            }

                            if (modifier) {
                                data = modifier(data)
                            }

                            this.setState({ [stateKey]: data })

                        }

                    }
                }, 100)

            } catch (e) {
                reject(e)
            }
        })

    }

    /**
     * get mx list attribute promise and reslove it also set it to the state if a state key is provided
     *
     * @param {*} list
     * @param {*} stateKey
     * @returns
     * @memberof MilestoneChartWidget
     */
    async loadItems(list, stateKey) {

        const items = [];
        for (let index = 0; index < list.length; index++) {
            const item = list[index];
            const itemWithDate = await getObject(item.id);
            const loadWithToday = this.showToday(itemWithDate.jsonData);
            items.push({ ...itemWithDate.jsonData.attributes, guid: item.id, index, loadWithToday });
        }

        if (stateKey) {
            this.setState({
                [stateKey]: items
            });
        }

        return { items, stateKey: stateKey ? stateKey : null }
    }

    /**
     * look for the milestone attributes and if not found look for it in the association 
     *
     * @param {*} items
     * @returns
     * @memberof MilestoneChartWidget
     */
    async getMileStoneAtrributesById(items) {

        const loadedItems = {}

        for (let index = 0; index < items.length; index++) {

            const item = items[index];

            if (item.MilestoneId.value !== null) {

                if (loadedItems.hasOwnProperty(item.MilestoneId.value)) {
                    loadedItems[item.MilestoneId.value].push(item)

                } else {
                    loadedItems[item.MilestoneId.value] = [
                        item
                    ]
                }

            } else {

                let attributesId = item["Integration.Attribute_Attributes"].value;

                const attributeAttributes = await getObject(attributesId);

                if (attributeAttributes !== null) {
                    const attributeAttributesId = attributeAttributes.jsonData.attributes["Integration.Attributes_Milestone"].value;
                    const attributesMilestone = await getObject(attributeAttributesId)
                    const milestoneId = attributesMilestone.jsonData.attributes.MilestoneId.value

                    if (loadedItems.hasOwnProperty(milestoneId)) {
                        loadedItems[milestoneId].push(item)

                    } else {
                        loadedItems[milestoneId] = [
                            item
                        ]
                    }
                }
            }

        }


        this.setState({
            attributesLoaded: true,
            attributes: loadedItems
        })

        return loadedItems;

    }

    componentDidMount() {

        this.getItemsFromListWhenReady("MileStoneChartData", "timeline", true);

        this.getItemsFromListWhenReady("allowedAtrributes", "allowedAtrributes", false, ",", (data) => {
            return data.reduce((acc, item) => {
                acc.push(item.toUpperCase().replace(/ /g, "_"))
                return acc;
            }, [])
        });


        const mileStoneMetadataLoader = setInterval(() => {
            const items = this.props.MileStoneMetaData.items;
            if (items) {
                const datas = items.map(r => getObject(r.id));
                Promise.all(datas).then(metadata => {
                    const sortedMetaData = metadata.reduce((acc, item) => {
                        const attrs = item.jsonData.attributes;
                        console.log("attrs", attrs)
                        acc[attrs.MilestoneId.value] = {
                            "Description": attrs.Description.value,
                            "IsInteractive": attrs.IsInteractive.value,
                            "UserRole": attrs.UserRole.value.split(",")
                        };

                        return acc;
                    }, {})
                    this.setState({
                        loaded: true,
                        metadata: sortedMetaData
                    });
                }).catch(console.warn);
                clearInterval(mileStoneMetadataLoader);
            }
        }, 150);



        window.onresize = () => {
            this.resizeWindow()
        };
    }


    resizeWindow() {
        // calculate frame height for PWA
        let scrollWrapper = document.querySelectorAll('.mx-scrollcontainer-wrapper');
        let contentContainer = scrollWrapper[1];
        let contentContainerHeight = parseInt(contentContainer.style.height.replace("px", ""));
        let height = (contentContainerHeight * 65) / 100;

        // small phones buffer
        if (window.innerHeight <= 667) {
            height = height - 50
        }

        console.log(`Resizing to ${height}`);
        this.containerDev.current.style.height = height;
    }

    // TO BE MOVED TO BACKEND AND CALLED AS A MICROFLOW
    async updateActualDate(id, actual_completion) {

        const styleid = this.props.StyleId.displayValue;
        const baseUrl = this.props.base_url.displayValue;
        const token = this.props.token.displayValue;
        const data = { actual_completion };

        const response = await fetch(`${baseUrl}/milestones/styles/${styleid}/milestones/${id}/dates`, {
            method: "PATCH",
            headers: {
                "Authorization": `Bearer ${token}`,
                "access-token": token,
                "x-ibm-client-id": "26adb636-34a4-41b2-80fb-e727af79111a",
                "Content-Type": "application/json"
            },
            body: JSON.stringify(data)
        });

        return await response.json();

    }
    /**
     * get Icon Style for the timeline icon
     *
     * @param {*} MilestoneId
     * @param {*} data
     * @returns
     * @memberof MilestoneChartWidget
     */
    getStyle(MilestoneId, data) {
        const metaData = this.state.metadata[MilestoneId];
        let patchedActualDate = false

        if (this.state.patchedMilestones) {
            if (this.state.patchedMilestones.hasOwnProperty(MilestoneId)) {
                patchedActualDate = this.state.patchedMilestones[MilestoneId]
            }
        }

        if (metaData) {
            if (metaData.IsInteractive) {

                if (patchedActualDate) {
                    return { background: "#eee", color: "#333", boxShadow: "none", height: 70 };
                } else {
                    if (data !== null && data !== "") {
                        return { background: "#eee", color: "#333", boxShadow: "none", height: 70 };
                    }
                }

                return { background: '#fff', color: '#333' };
            } else {
                return { background: "#eee", color: "#333", boxShadow: "none", height: 70 };

            }
        }

        return { background: "#eee", color: "#333", boxShadow: "none", height: 70 };

    }
    /**
     * Generate milestone icon based on the status and the date
     *
     * @param {*} MilestoneId
     * @param {*} status
     * @param {*} date
     * @param {*} revisedDate
     * @param {*} scheduleCompletion
     * @returns
     * @memberof MilestoneChartWidget
     */
    getIcon(MilestoneId, status, date, revisedDate, scheduleCompletion) {
        const metaData = this.state.metadata[MilestoneId];
        let patchedActualDate = false;

        if (this.state.patchedMilestones) {
            if (this.state.patchedMilestones.hasOwnProperty(MilestoneId)) {
                patchedActualDate = this.state.patchedMilestones[MilestoneId]
            }
        }

        if (metaData) {
            if (metaData.IsInteractive) {
                if (patchedActualDate) {
                    return <IconDate
                        status={status}
                        date={patchedActualDate || date}
                        revisedDate={patchedActualDate}
                        scheduleCompletion={patchedActualDate}
                        onClick={() => this.handleOpenModal(MilestoneId)}
                    />
                } else {
                    if (date !== null && date !== "") {
                        return <IconDate
                            status={status}
                            date={date}
                            revisedDate={revisedDate}
                            scheduleCompletion={scheduleCompletion}
                        />

                    }
                }
                return <FaCalendarPlus onClick={() => this.handleOpenModal(MilestoneId)} />
            } else {

                return <IconDate
                    status={status}
                    date={date}
                    revisedDate={revisedDate}
                    scheduleCompletion={scheduleCompletion}
                />

            }
        }
        return <IconDate
            status={status}
            date={date}
            revisedDate={revisedDate}
            scheduleCompletion={scheduleCompletion}
        />

    }

    showToday(milestone) {
        const { attributes } = milestone

        try {
            const actualCompletion = attributes.ActualCompletion.value;

            const scheduleCompletion = attributes.ScheduleCompletion.value;

            const actualDate = actualCompletion === null ? scheduleCompletion.split("-") : actualCompletion.split("-");
            const day = actualDate[2];
            const month = Months[actualDate[1]];;
            const year = actualDate[0];

            var CurrentDate = new Date();
            var GivenDate = new Date(`${month} ${day}, ${year}`)

            if (GivenDate > CurrentDate && !this.state.todayRendered) {
                this.setState({ todayRendered: true });
                return {
                    show: true,
                    mon: CurrentDate.getShortMonthName(),
                    day: CurrentDate.getDate()
                };

            } else {
                return { show: false };
            }

            return { show: false };
        } catch (e) {

            return { show: false };
        }
    }


    handleOpenModal = (milestoneId) => {

        this.setState({ showModal: true, activeMilestone: milestoneId });
    }

    handleCloseModal() {
        this.setState({ showModal: false });
    }

    showLocations(attributes) {
        this.setState({ showLocations: attributes })
    }

    changeDate(date) {
        this.handleCloseModal();
        const day = date.getDate().toString().length === 1 ? `0${date.getDate()}` : date.getDate();
        const month = date.getMonth().toString().length === 1 ? `0${date.getMonth()}` : date.getMonth();
        const wpMonth = date.getMonth().toString().length === 1 ? `0${date.getMonth() + 1}` : date.getMonth() + 1;
        const year = date.getFullYear();
        const actualDate = `${year}-${month}-${day}`;
        const wpActualDate = `${year}-${wpMonth}-${day}`;

        this.updateActualDate(this.state.activeMilestone, wpActualDate).then(() => {
            if (this.state.patchedMilestones) {
                this.setState((state) => ({
                    patchedMilestones: {
                        ...state.patchedMilestones,
                        [state.activeMilestone]: wpActualDate
                    },
                    activeMilestone: false
                }))
            } else {
                this.setState((state) => ({
                    patchedMilestones: {
                        [state.activeMilestone]: wpActualDate
                    },
                    activeMilestone: false
                }))
            }
        }).catch(console.log)
    }


    renderToday(show, attributes) {

        if (show) {
            return (
                <VerticalTimelineElement
                    className="today"
                    contentStyle={{ background: "#eee", boxShadow: "none", border: 0, paddingTop: "2rem" }}
                    iconStyle={{ background: "#eee", color: "#333", boxShadow: "none", height: 70 }}
                    icon={
                        <TodayIcon {...attributes.loadWithToday} />
                    }
                >
                    <div style={{ display: "flex" }}>
                        <div style={{ color: "#0082ff", fontWeight: "bold" }}>Today</div>
                        <div
                            style={{
                                background: "#0082ff",
                                height: "0.2rem",
                                width: "100%",
                                marginLeft: "10px",
                                marginTop: "10px"
                            }}
                        ></div>
                    </div>
                </VerticalTimelineElement>
            )
        }
    }

    checkIfneedsSpliting(item) {
        if (item.indexOf(',') >= 0) {

            return (
                <React.Fragment>
                    <ol style={{ paddingLeft: "4rem", listStyle: "disc" }}>
                        {
                            item.split(',').map(s => <li>{s}</li>)
                        }
                    </ol>
                </React.Fragment>
            )
        }

        return item
    }

    render() {
        const { loaded, attributesLoaded, metadata, timeline } = this.state;

        // display a loading bar if items still not loaded
        if (!loaded) {
            return (
                <div>
                    <br /><br />
                    <center>
                        <Loader
                            type="Puff"
                            color="#3cb233"
                            height={100}
                            width={100}
                            timeout={5000}
                        />
                    </center>
                </div>)
        }

        // Load Attributes After Getting Milestones
        if (!attributesLoaded && loaded) {

            this.getItemsFromListWhenReady("Attributes", "attributes", true)
                .then(({ items }) => {

                    this.getMileStoneAtrributesById(items);
                });
        }

        
        // if Last Milesone is click it need to show the locations only.
        if (this.state.showLocations) {

            // hide main back button
            let mainBkBtn = document.querySelector('.main-back-btn');
            mainBkBtn.style.display = "none";

            // hide tabs 
            let tabs = document.querySelector('.nav-tabs');
            tabs.style.display = "none";

            // calculate frame height for PWA
            let scrollWrapper = document.querySelectorAll('.mx-scrollcontainer-wrapper');
            let contentContainer = scrollWrapper[1];
            let contentContainerHeight = parseInt(contentContainer.style.height.replace("px", ""));
            let height = (contentContainerHeight * 65) / 100;

            // small phones buffer
            if (window.innerHeight <= 667) {
                height = height - 50
            }

            // Format location attributes for supplierASN Milesone

            let attr = this.state.attributes["supplierasn_import"]
            let clist = {};

            attr.forEach(item => {
                if (item.Name.value === "Location") {
                    if (!Number.isInteger(item.Value.value)) {
                        clist[item.Value.value] = {}
                    }
                }
            });

            let clistwithattr = attr.reduce((acc, item) => {
                const location = item.Location.value;

                if (location !== null && !Number.isInteger(location)) {
                    if (acc.hasOwnProperty(location)) {
                        acc[location][item.Name.value] = item.Value.value
                    }
                }

                return acc;
            }, { ...clist });

           
            return (

                <div>
                    <br />
                    <button onClick={() => this.setState({ showLocations: false })}><FaArrowLeft size={'1.5em'} /></button>
                    <div style={{ background: "#eeeeee", overflow: "scroll", height }}>
                        <ul style={{ marginTop: "1rem" }}>

                            {Object.keys(clistwithattr).map(item => {
                                return (
                                    <li style={styles.location}>
                                        <div style={{ position: "relative" }}>
                                            <h3 style={{
                                                fontSize: "1.3em",
                                                fontWeight: "800",
                                                color: "#565656"
                                            }}>{item}</h3>
                                            <h4 style={{ color: "#555", fontWeight: "800", marginTop: "1rem" }}>Current Status: {'  '}{clistwithattr[item]["Current Status"]}</h4>
                                            <br />
                                            <ul>
                                                {
                                                    Object.keys(clistwithattr[item]).map(atr => {
                                                        if (atr !== "Current Status" && atr !== "Next Action" && atr !== "role") {
                                                            return <li>{atr}{'  '} {this.checkIfneedsSpliting(clistwithattr[item][atr])}</li>
                                                        }
                                                    })
                                                }
                                            </ul>
                                            <br />
                                            <hr />
                                            <span style={styles.locationDate}>Next Action:{'  '} {clistwithattr[item]["Next Action"]}</span>

                                            <br />
                                            {this.props.canNotify.displayValue === "Yes" && <MileStoneNotification {...this.props}
                                                metadata={{ ...this.state.metadata[this.state.showLocations.MilestoneId.value], UserRole: [clistwithattr[item].role] }}
                                                milestone={{ ...this.state.showLocations, StyleLMId: this.props.StyleLMId, "StyleId": this.props.StyleId, "SeasonID": this.props.seasonID }}
                                                HitID={this.props.HitID}
                                                StyleName={this.props.StyleName.displayValue}

                                                conceptId={this.props.ConceptId}
                                            />}
                                        </div>
                                    </li>
                                )
                            })}
                        </ul>
                    </div>
                </div>
            )
        }

        // show main back button
        let mainBkBtn = document.querySelector('.main-back-btn');
        mainBkBtn.style.display = "block";

        // show tabs
        let tabs = document.querySelector('.nav-tabs');
        tabs.style.display = "block";

        // calculate frame height for PWA
        let scrollWrapper = document.querySelectorAll('.mx-scrollcontainer-wrapper');
        let contentContainer = scrollWrapper[1];
        let contentContainerHeight = parseInt(contentContainer.style.height.replace("px", ""));
        let height = (contentContainerHeight * 65) / 100;

        // small phones buffer
        if (window.innerHeight <= 667) {
            height = height - 50
        }

        return (
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <div style={{ background: "#eeeeee", overflow: "scroll", height }} ref={this.containerDev}>
                    {
                        timeline.length > 0 && <h1
                            className="actual-title"
                            style={{
                                color: "#4a4a4a",
                                fontWeight: "normal",
                                fontSize: "1rem",
                                marginLeft: "0.7rem",
                                marginBottom: "1rem"
                            }}
                        >
                            ACTUAL
                </h1>
                    }
                    <React.Fragment>
                        <div style={{ display: "none" }}>
                            <DatePicker
                                label="Basic example"
                                value={this.state.selectedDate}
                                onChange={(date) => this.changeDate(date)}
                                animateYearScrolling
                                autoOk
                                open={this.state.showModal}
                                onClose={() => this.setState({ showModal: false })}
                            />
                        </div>
                    </React.Fragment>

                    <VerticalTimeline animate={false}>
                        {
                            metadata && timeline.map(attributes => {
                                return (
                                    <React.Fragment>
                                        {this.renderToday(attributes.loadWithToday.show, attributes)}

                                        <VerticalTimelineElement
                                            className="vertical-timeline-element--work"
                                            contentStyle={{ background: "#fff", color: "#333" }}
                                            contentArrowStyle={{ borderRight: "7px dotted  #fff" }}
                                            date={beautify(attributes.ScheduleCompletion.value)}
                                            status={attributes.Status.value.replace('_', " ")}
                                            iconStyle={
                                                this.getStyle(attributes.MilestoneId.value, attributes.ActualCompletion.value)
                                            }
                                            icon={

                                                this.getIcon(
                                                    attributes.MilestoneId.value,
                                                    attributes.Status.value,
                                                    attributes.ActualCompletion.value,
                                                    attributes.RevisedCompletion.value,
                                                    attributes.ScheduleCompletion.value
                                                )

                                            }
                                        >

                                            <div style={{ display: "flex" }}>
                                                <div>
                                                    <img src={`/milestone_icons/${attributes.MilestoneId.value}.svg`} style={{
                                                        marginRight: "1.2rem"
                                                    }} />
                                                </div>
                                                <div>
                                                    <h3 className="vertical-timeline-element-title" style={{ color: attributes.Status.value === "RISK" ? COLORS.red : COLORS.green }}>
                                                        {this.state.metadata.hasOwnProperty(attributes.MilestoneId.value) ? this.state.metadata[attributes.MilestoneId.value].Description : `${attributes.MilestoneId.value.replace(/_/g, " ")}`}
                                                    </h3>
                                                </div>
                                            </div>


                                            {
                                                attributesLoaded && this.state.attributes.hasOwnProperty(attributes.MilestoneId.value) && <MileStoneAttributes
                                                    attributes={this.state.attributes[attributes.MilestoneId.value]}
                                                    metadata={this.state.metadata[attributes.MilestoneId.value]}
                                                    HitID={this.props.HitID}
                                                    StyleName={this.props.StyleName.displayValue}
                                                    axtraAttrs={{
                                                        current_status: this.props.asn_supplier_current_status.displayValue,
                                                        next_action: this.props.asn_supplier_next_status.displayValue,
                                                    }}
                                                    allowedAtrributes={this.state.allowedAtrributes}
                                                    canNotify={this.props.canNotify}
                                                    conceptId={this.props.ConceptId}
                                                    token={this.props.token}
                                                    milestone={{ ...attributes, StyleLMId: this.props.StyleLMId, "StyleId": this.props.StyleId, "SeasonID": this.props.seasonID }}
                                                    showLocations={() => this.showLocations(attributes)}
                                                    resizeWindow={() => this.resizeWindow()}
                                                />


                                            }

                                        </VerticalTimelineElement>
                                    </React.Fragment>
                                )
                            })
                        }

                    </VerticalTimeline>
                </div>
            </MuiPickersUtilsProvider>
        );
    }
}



const styles = {
    location: {
        background: "#fff",
        padding: "2rem",
        borderBottom: "1px solid #ccc"

    },
    locationDate: {
        fontSize: "1.2rem",
        marginTop: "1rem",
        display: "block",
        fontWeight: "600",
        color: "#565656"
    },
    riskTab: {
        background: "rgb(188, 43, 43)",
        color: "rgb(255, 255, 255)",
        padding: "0.5rem",
        borderRadius: "50px",
        width: "75px",
        display: "block",
        textAlign: "center",
        position: "absolute",
        top: "0px",
        right: "0px"
    },
    moderateTab: {
        background: "orange",
        color: "rgb(255, 255, 255)",
        padding: "0.5rem",
        borderRadius: "50px",
        width: "100px",
        display: "block",
        textAlign: "center",
        position: "absolute",
        top: "0px",
        right: "0px"
    }
}

export default hot(MilestoneChartWidget);
