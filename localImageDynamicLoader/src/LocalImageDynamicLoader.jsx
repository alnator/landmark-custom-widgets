import { Component, createElement } from "react";
import { hot } from "react-hot-loader/root";

import "./ui/LocalImageDynamicLoader.css";

function LocalImageDynamicLoader(props) {
    const { width, height, imageName, imagePath, imageExtention } = props;
    return (
        <div className="localImage">
            <img style={{ width, height }} src={`${imagePath}/${imageName.displayValue}.${imageExtention}`} />
        </div>
    );
}

export default hot(LocalImageDynamicLoader);
