import React,{ Component, createElement } from "react";
import { hot } from "react-hot-loader/root";

import Lottie from 'react-lottie';
import "./ui/MxLottie.css";

import animationData from './lotties/notification.json'



class MxLottie extends Component {
    constructor(props) {
        super(props);

        this.state = {isStopped: false, isPaused: false};

        this.onClickHandler = this.onClick.bind(this);
    }

    render() {


        const defaultOptions = {
            loop: true,
            autoplay: true, 
            animationData: animationData,
            rendererSettings: {
              preserveAspectRatio: 'xMidYMid slice'
            }
          };


        return (
            <React.Fragment>
                
            <Lottie options={defaultOptions}
              height={parseInt(this.props.width) || 300}
              width={parseInt(this.props.height) || 300}
              isStopped={this.state.isStopped}
              isPaused={this.state.isPaused}
              />
              <center>
                <h6 style={{
                    color: "#787d91",
                    fontSize: "1.2rem"
                }}>No Notifications</h6>
              </center>
              </React.Fragment>
        );
    }

    onClick() {
        if (this.props.onClickAction && this.props.onClickAction.canExecute) {
            this.props.onClickAction.execute();
        }
    }
}

export default hot(MxLottie);
