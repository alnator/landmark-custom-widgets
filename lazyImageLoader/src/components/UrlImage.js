import React, { createElement } from "react";
import { LazyLoadImage } from "react-lazy-load-image-component";

const UrlImageLazyLoader = ({ alt, height, width, url }) => (
    <div className="lazy-image-loader-container">
        <LazyLoadImage alt={alt} height={height} src={url} width={width} />
    </div>
);

export default UrlImageLazyLoader;
