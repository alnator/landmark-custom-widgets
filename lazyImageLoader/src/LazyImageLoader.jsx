import { Component, createElement } from "react";
import { hot } from "react-hot-loader/root";
import fetchWithTimeout from './components/fetchWithTimeout';
import UrlImageLazyLoader from "./components/UrlImage";
import "./ui/LazyImageLoader.css";

class LazyImageLoader extends Component {
    constructor(props) {
        super(props);
        this.state = {
            objectURL: false
        }
        this.onClickHandler = this.onClick.bind(this);
    }

    
    // fetchImage(url, token, concept, base_service_url, media_user_id, media_password, ibm_client_id) {

    //     fetchWithTimeout(`https://apidev.landmarkgroup.com/landmarkgroup/retail-dev/v2/sahla-buy/milestones/media/concepts/${concept}?mediaUrl=${url}`, {
    //         method: "GET",
    //         mode: 'cors',
    //         headers: {
    //             "Authorization": `Bearer ${token}`,
    //             "x-ibm-client-id": "26adb636-34a4-41b2-80fb-e727af79111a",
    //             user_id: "STEP",
    //             pswd: "sahlabuy@2019",
    //             "access-token": token
    //         }
    //     }, 500)
    //         .then(response => response.blob())
    //         .then((myBlob) => {
    //             console.log(myBlob)
    //             var objectURL = URL.createObjectURL(myBlob);
    //             this.setState({ objectURL });
    //         });

    // }

    fetchImage(url, token, concept, base_service_url, media_user_id, media_password, ibm_client_id, timeout) {

        fetchWithTimeout(`${base_service_url}/milestones/media/concepts/${concept}?mediaUrl=${url}`, {
            method: "GET",
            mode: 'cors',
            headers: {
                "Authorization": `Bearer ${token}`,
                "x-ibm-client-id": ibm_client_id,
                "user_id": media_user_id,
                "pswd": media_password,
                //"access-token": token
            }
        }, parseInt(timeout || "10000"))
       
            .then(response => response.blob())
            .then((myBlob) => {
                // console.log(myBlob)
                var objectURL = URL.createObjectURL(myBlob);
                this.setState({ objectURL });
            }).catch(e => {
                this.setState({ objectURL: "/img/LaunchTracker$Images$no_image.png" });
            });

    }

    render() {
        const { imageURL, token, concept, width, height, base_service_url, media_user_id, media_password, ibm_client_id, timeout } = this.props;

        if (this.state.objectURL) {
            return (
                <div className="widget-lazyimageloade">
                    <UrlImageLazyLoader
                        alt="Image"
                        url={this.state.objectURL}
                        width={width || "100%"}
                        height={height || "100%"}
                        onClickAction={this.onClickHandler}
                        style={{
                            borderRadius: "15px",
                            border: " 2px solid #ccc"
                        }}
                    />
                </div>
            );
        } else {

            if (imageURL.status === "available" && token.status === "available" && concept.status === "available") {
                this.fetchImage(
                    imageURL.displayValue,
                    token.displayValue,
                    concept.displayValue,
                    base_service_url.displayValue,
                    media_user_id.displayValue,
                    media_password.displayValue,
                    ibm_client_id.displayValue,
                    timeout.displayValue
                )
            }

            return (
                <div className="widget-lazyimageloade">
                    <img src={'/loading.gif'} width={60} height={60} style={{
                        borderRadius: "15px",
                        border: " 2px solid #ccc"
                    }} />
                </div>
            )

        }
    }

    onClick() {
        if (this.props.onClickAction && this.props.onClickAction.canExecute) {
            this.props.onClickAction.execute();
        }
    }
}

export default hot(LazyImageLoader);
